# Como instalar:
O [NodeJS](https://nodejs.org/en/) é preciso estar instalado e é um requisito.

Como os ficheiros e o html são servidos com o [Webpack 4](https://webpack.js.org):
- e este necessita da versão de Node >= 8.9.4.
- usar o [Node Version Manager](https://github.com/creationix/nvm) (nvm) caso seja necessário mudar a versão do Node que está a ser usada.

Depois de Clonar ou fazer o [Download](https://gitlab.com/pompalini1/ticket-component/-/archive/master/ticket-component-master.zip) e unzip, 
na pasta resultante e na linha de comandos correr:

    $ npm install && npm start

Depois da instalação completar um browser vai abrir com: [http://localhost:9001](http://localhost:9001)

Para correr os testes:

    $ npm test


<br><br>
# Tarefa:
Desenvolver este componente, com layout o mais semelhante possível:

![componente](/uploads/fde930f7fce104bc702e4f16fb076c9a/componente.png)

## Pressupostos do componente:
1. Por default, o campo está preenchido com `1 Adulto`;
1. A seleção feita dentro da tooltip deve ser mostrada a modo de resumo no campo, por exemplo:

        "2 Adultos, 1 Criança";
        "1 Adulto, 1 Bebé";
        "3 Adultos, 2 Crianças, 1 Bebé";

    **Nota**: Será também necessário respeitar os termos no plural e singular.

1. Ao carregar no campo, este passa ao estado aberto;
1. Estando o campo no estado aberto, ao clicar fora do campo ele passa ao estado Default;
1. Quando a regra é invalidada os botões de adicionar ou remover devem ficar desabilitados para cada caso em particular;
1. Este componente deve conseguir conviver com mais componentes iguais na mesma página, trabalhando de forma isolada dos restantes.

## Regras a aplicar:
Não é possível selecionar mais do que 9 passageiros;

Por cada adulto é possível:
1. Adulto, s/ bebés, pode adicionar até 4 crianças;
1. Adulto, c/ entre 0 - 1 crianças, pode adicionar 1 bebé; 
1. Adulto, c/ 2 ou mais crianças, não pode adicionar bebés.

**Nota**: A aplicação de acessibilidade neste módulo, será valorizada. 
