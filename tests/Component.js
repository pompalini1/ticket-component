import "@babel/polyfill"; // Babel Object Polyfill to fix Object.assign call
import TicketsSelector from '../src/components/TicketsSelector';

let component_values = {
  options: {
    maxTickets: 9,
    total: {
      adults: 1,
      children: 0,
      babies: 0,
    },
  },
  selectors: [
    {
      title: 'Adultos (+12 anos)',
      type: 'adults',
      singular: 'Adulto',
      plural: 'Adultos',
      value: 1,
      minValue: 1,
    },
    {
      title: 'Crianças (2-11 anos)',
      type: 'children',
      singular: 'Criança',
      plural: 'Crianças',
      value: 0,
      minValue: 0,
      maxValue: 4
    },
    {
      title: 'Bebés (-2 anos)',
      type: 'babies',
      singular: 'Bebé',
      plural: 'Bebés',
      value: 0,
      minValue: 0,
      maxValue: 1
    },
  ]
};

let component;

beforeEach(() =>{
  component = new TicketsSelector(component_values);
});

describe("When a component is created", function () {
  it("the state should be set to the default values", function () {
    expect(component.state).toEqual(component_values);
  });
});

describe("When adding tickets", function () {
  it("maximum total tickets value is 9", function () {
    const value = component.state.options.maxTickets;
    component.state.options.total.adults = value;
    component.state.selectors[0].value = value;
    const adults = component.validatedValue(value, 1, component.state.selectors[0]);
    expect(adults).toEqual(value);
    component.state.options.total.adults = 5;
    component.state.selectors[0].value = 5;
    component.state.options.total.children = 4;
    component.state.selectors[1].value = 4;
    const babies = component.validatedValue(0, 1, component.state.selectors[2]);
    expect(babies).toEqual(0);

  });
  it("minimum adults is 1", function () {
    const value = 1;
    component.state.options.total.adults = value;
    component.state.selectors[0].value = value;
    const adults = component.validatedValue(value, -1, component.state.selectors[0]);
    expect(adults).toEqual(value);
  });
  it("minimum children and babies is 0", function () {
    const value = 0;
    component.state.options.total.children = value;
    component.state.options.total.babies = value;
    component.state.selectors[1].value = value;
    component.state.selectors[2].value = value;
    const children = component.validatedValue(value, -1, component.state.selectors[1]);
    const babies = component.validatedValue(value, -1, component.state.selectors[2]);
    expect(children).toEqual(value);
    expect(babies).toEqual(value);
  });
  it("maximum babies is 1", function () {
    const maxValue = 1;
    component.state.options.total.babies = maxValue;
    component.state.selectors[2].value = maxValue;
    const value = component.validatedValue(maxValue, 1, component.state.selectors[2]);
    expect(value).toEqual(maxValue);
  });
  it("maximum children is 4", function () {
    const maxValue = 4;
    component.state.options.total.children = maxValue;
    component.state.selectors[1].value = maxValue;
    const value = component.validatedValue(maxValue, 1, component.state.selectors[1]);
    expect(value).toEqual(maxValue);
  });
  it("maximum children is 1 if has 1 baby", function () {
    const maxValue = 1;
    component.state.options.total.children = maxValue;
    component.state.options.total.babies = maxValue;
    component.state.selectors[1].value = maxValue;
    const value = component.validatedValue(maxValue, 1, component.state.selectors[1]);
    expect(value).toEqual(maxValue);
  });
});