import App from './components/App';


function component() {
  // create our App here
  const element = new App();
  return element;
};

const container = document.getElementById('root');
container.appendChild(component());