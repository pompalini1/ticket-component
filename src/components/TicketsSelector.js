import './TicketsSelector.sass';
import Dropdown from './Dropdown';

// the id to add to the parent's Component element
let firstId = 1;

/**
 * This is the class that inherits all the others and creates the selectors with Dropdown > Component.
 * @class
 */
export default class TicketsSelector extends Dropdown {
  /**
   * @param {object} state - The component's state.
   */
  constructor(state) {
    super(state, firstId++);
    this.state = Object.assign({}, this.state, state);
    this.options = state.options;
  }

  /**
   * updateAddedElement update the clicked selector values and state
   * @param {htmlDOMElement} button - the clicked button.
   * @param {string} action - can be increase or decrease.
   * @param {object} props - the clicked selector's properties.
   */
  updateAddedElement(button, action, {...props}) {
    const addedElement = button.closest('.buttons').querySelector('.added'); // reference the element to be updated
    const elementValue = parseInt(addedElement.innerText, 10);
    const selectorValue = action === 'increase' ? 1 : -1; // depending on action we will increase or decrease value
    const value = this.validatedValue(elementValue, selectorValue, props);

    // first enable all buttons
    this.verifyButtonState(button);

    // update the text element with new value
    if (addedElement) {
      addedElement.innerText = value;
    }

    // now update the state
    this.setState(value, props);
  }

  /**
   * setState set's the local and the parent states 
   * @param {number} value - the added value.
   * @param {object} props - the added selector's properties.
   */
  setState(value, { ...props }) {
    // update the local state
    this.state.selectors.map(selectorValue => {
      if (selectorValue.type === props.type) {
        selectorValue.value = value;
      }
    });
    // send the state to component, we need to update it's text
    super.setState(this.state);
  }

  /**
   * getTotalCount and enable or disable all buttons accordingly
   * @param {htmlDOMElement} button - the clicked button.
   * @return {number} the total added count.
   */
  getTotalCount(total) {
    let totalCount = 0;

    for (const key in total) {
      if (total.hasOwnProperty(key)) {
        const val = parseInt(total[key]);
        totalCount += val;
      }
    }
    // TODO: as we are in a get method we should not update the totalValue here
    return this.options.totalValue = totalCount;
  }

  /**
   * verifyButtonState and enable or disable all buttons accordingly
   * FIXME: this method is not very DRY, rinse it off please!
   * @param {htmlDOMElement} button - the clicked button.
   */
  verifyButtonState(button) {
    const buttonsElement = button.closest('.dropdown');
    const total = this.options.total;
    const totalCount = this.options.totalValue;
    // first enable all
    this.toggleButtonsState(buttonsElement, 'button', true);

    if (total.adults <= 1) {
      this.toggleButtonsState(buttonsElement, '[data-type="adults"] [data-action="decrease"]', false);
    } else {
      this.toggleButtonsState(buttonsElement, '[data-type="adults"] [data-action="decrease"]', true);
    }

    if (total.babies < 1) {
      this.toggleButtonsState(buttonsElement, '[data-type="babies"] [data-action="decrease"]', false);
    } else {
      this.toggleButtonsState(buttonsElement, '[data-type="babies"] [data-action="decrease"]', true);
    }

    if (total.children < 1) {
      this.toggleButtonsState(buttonsElement, '[data-type="children"] [data-action="decrease"]', false);
    } else {
      this.toggleButtonsState(buttonsElement, '[data-type="children"] [data-action="decrease"]', true);
    }

    if (totalCount >= 9) {
      this.toggleButtonsState(buttonsElement, '[data-action="increase"]', false);
    }

    if (total.children > 1) {
      this.toggleButtonsState(buttonsElement, '[data-type="babies"] [data-action="increase"]', false);
    }

    if (total.children >= 4) {
      this.toggleButtonsState(buttonsElement, '[data-type="children"] [data-action="increase"]', false);
    }

    if (total.babies >= 1) {
      this.toggleButtonsState(buttonsElement, '[data-type="babies"] [data-action="increase"]', false);
      if (total.children >= 1) {
        this.toggleButtonsState(buttonsElement, '[data-type="children"] [data-action="increase"]', false);
      } else {
        this.toggleButtonsState(buttonsElement, '[data-type="children"] [data-action="increase"]', true);
      }
    }
  }

  /**
   * toggleButtonsState to enable or disable buttons
   * @param {htmlDOMElement} element - the element in which we will query it's DOM.
   * @param {string} cssSelector - the string to be queried.
   * @param {boolean} enable - to enable or disable button.
   */
  toggleButtonsState(element, cssSelector, enable) {
    const buttons = element.querySelectorAll(cssSelector);

    for (let i = 0; i < buttons.length; i++) {
      const button = buttons[i];
      if (enable) {
        button.removeAttribute('disabled');
      } else {
        button.setAttribute('disabled', '');
      }
    }
  }

  /**
   * validatedValue return values after certain conditions are met:
   * 1: adults cannot be < 1 (minValue)
   * 2: for all others cannot be < 0
   * 3: cannot add more than 9 passengers
   * 4: with 0 or 1 children can only add 1 baby
   * 5: with 1 or more children cannot add babies
   * @param {number} elementValue - the clicked selector added element current value.
   * @param {string} value - the value to increase or decrease: 1 or -1.
   * @param {object} selector - the clicked selector properties.
   * @return {number} the number value after validations.
   */
  validatedValue(elementValue, value, {...selector}) {
    const total = this.options.total;
    const localValue = this.options.total[selector.type] = elementValue + value;
    const totalCount = this.getTotalCount(total);
    const hasMaxPassengers = totalCount > this.options.maxTickets;
    const hasMinValue = localValue < selector.minValue;
    const hasMaxChildrenWithNoBabies = selector.type === 'children' && total.babies === 0 && total.children > selector.maxValue; 
    const hasMaxChildrenWithBabies = total.babies > 0 && total.children > 1;
    const hasMaxBabies = selector.type === 'babies' && total.babies > selector.maxValue;
    const updateValue = (value) => this.options.total[selector.type] = value;

    // cannot be less than the minValue
    if (hasMinValue) {
      return updateValue(elementValue);
    }
    // can only add 9 passengers
    if (hasMaxPassengers) {
      return updateValue(elementValue);
    }

    // can only add 1 baby no matter what
    if (hasMaxBabies) {
      return updateValue(elementValue);
    }

    // can only add 4 children if has no babies
    if (hasMaxChildrenWithNoBabies) {
      return updateValue(elementValue);
    }

    // can only add 1 children with 1 baby
    if (hasMaxChildrenWithBabies) {
      return updateValue(elementValue);
    }

    return updateValue(localValue);
  }

  /**
   * Call's updateAddedElement
   * @param {object} event - the clicked dropdown event.
   * @param {string} actions - the clicked dropdown event.
   * @param {object} props - the clicked selector properties.
   */
  handleSelectorClick(event, action, {...props}) {
    this.updateAddedElement(event.target, action, { ...props });
  }

  /**
   * Call's the parents (Dropdown) html method to render it's html
   * @param {object} event - the cliked dropdown event.
   */
  handleDropddownClick(event) {
    super.showHideDropdown(event.target);
  }

  /**
   * Call's the parents (Dropdown) html method and render's the whole thing
   * @return {htmlDOMElement} the selector's html.
   */
  render(){
    const selectorHtml = this.state.selectors.map(selector => {
      return this.html({ ...selector});
    });
    return super.html(selectorHtml);
  }

  /**
   * Render the TicketsSelector
   * @param {object} props - the selector's properties.
   * @return {htmlDOMElement} the selector's html.
   */
  html({...props}) {
    const list = document.createElement('ul');
    const title = document.createElement('li');
    const buttons = document.createElement('li');
    const added = document.createElement('div');
    const minusButton = document.createElement('button');
    const plusButton = document.createElement('button');
    const minus = document.createElement('div');
    const plus = document.createElement('div');

    plusButton.className = 'button rounded-corners';
    plusButton.setAttribute('type', 'button');
    plusButton.setAttribute('data-action', 'increase');
    plusButton.innerText = '+';
    minusButton.className = 'button rounded-corners';
    minusButton.setAttribute('type', 'button');
    minusButton.setAttribute('data-action', 'decrease');
    minusButton.setAttribute('disabled', '');
    minusButton.innerText = '-';
    list.className = 'selector';
    title.innerText = props.title;
    title.className = 'title';
    added.className = 'added';
    added.innerText = props.value;
    buttons.className = 'buttons';

    plusButton.onclick = (event) => {
      this.handleSelectorClick(event, 'increase', {...props});
    }

    minusButton.onclick = (event) => {
      this.handleSelectorClick(event, 'decrease', { ...props });
    }

    plus.appendChild(plusButton);
    minus.appendChild(minusButton);

    buttons.appendChild(minus);
    buttons.appendChild(added);
    buttons.appendChild(plus);

    list.appendChild(title);
    list.appendChild(buttons);
    list.setAttribute('data-type', props.type);

    return list;
  }
}