import './Component.sass';
import userIcon from '../assets/icons/user.svg';

/**
 * Creates a new Component.
 * @class
 */
export default class Component {
  /**
   * @param {object} state - The component's state.
   * @param {number} id - The id value to create a unique component's id attribute, is set in TicketsSelector;
   */
  constructor(state, id){
    this.state = Object.assign({}, this.state, state);
    this.id = id;
  }

  /**
   * Create the component's text with initial values.
   * @return {string} The text to be shown in the compenent element.
   */
  componentText() {
    const selector = this.state.selectors[0];
    const value = selector.value;
    return `${value} ${value > 1 ? selector.plural : selector.singular}`;
  }

  /**
   * Update the component's text with set values.
   * @return {string} The text to be shown in the compenent element.
   */
  updateComponentText(selectors, id) {
    const component = document.querySelector(`#ticket-component-${id} button`);
    let string = '';

    for (let index in selectors) {
      const selector = selectors[index];
      const type = selector.value > 1 ? selector.plural : selector.singular;

      if (selector.value > 0) {
        if (parseInt(index, 10) > 0) {
          string += ', ';
        }
        string += `${selector.value} ${type}`;
      }
    }

    // update component text if sting is not empty
    if (string) {
      component.innerHTML = string;
    }
  }

  /**
   * Update the component's state.
   * @param {object} state - The component's current state.
   */
  setState(state) {
    this.state = Object.assign({}, this.state, state);
    this.updateComponentText(this.state.selectors, this.id);
  }

  /**
   * Get the component's state.
   */
  getState() {
    return this.state;
  }

  /**
   * Render the component
   * @return {htmlDOMElement} the component's html.
   */
  html(){
    const component = document.createElement('div');
    const button = document.createElement('button');
    const icon = document.createElement('div');

    icon.className = 'icon user';
    icon.innerHTML = userIcon;

    component.className = 'ticket-component';
    component.id = 'ticket-component-' + this.id;
    component.appendChild(icon);

    button.className = 'btn rounded-corners';
    button.innerHTML = this.componentText();
    button.onclick = (event) => {
      this.handleDropddownClick(event);
    }


    component.appendChild(button);

    return component;
  }
}