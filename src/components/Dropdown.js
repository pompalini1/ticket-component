import './Dropdown.sass';
import Component from './Component';


/**
 * Creates a new Dropdown.
 * @class
 */
export default class Dropdown extends Component {
  /**
   * @function setState
   * @memberof Component
   * @static
  */
  static setState(state) {
    return super.setState(state);
  }

  /**
   * Show or hide for all the dropdown's
   */
  showHideDropdown() {
    const button = event.target;
    const component = button.parentNode;
    const dropdownEl = component.querySelector('.dropdown');
    const activeClass = 'active';

    checkClick(component);

    if (!hasClass(component, activeClass)) {
      addClass(component, activeClass);
    } else {
      removeClass(component, activeClass);
    }
  }

  /**
   * Render the Dropdown
   * @param {object} selectors - the element's to be added in the parent's component.
   * @return {htmlDOMElement} the dropdown's html.
   */
  html(selectors) {

    const dropdown = document.createElement('div');

    dropdown.className = 'dropdown rounded-corners';
    selectors.map(selector => {
      dropdown.appendChild(selector);
    });

    const parentHtml = super.html();
    parentHtml.appendChild(dropdown);

    return parentHtml;
  }
}