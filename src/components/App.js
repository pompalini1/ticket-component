import './App.sass';
import '../libs/helpers';
import TicketsSelector from './TicketsSelector';


let component_values = {
  options: {
    maxTickets: 9,
    total: {
      adults: 1,
      children: 0,
      babies: 0,
    },
  },
  selectors: [
    {
      title: 'Adultos (+12 anos)',
      type: 'adults',
      singular: 'Adulto',
      plural: 'Adultos',
      value: 1,
      minValue: 1,
    },
    {
      title: 'Crianças (2-11 anos)',
      type: 'children',
      singular: 'Criança',
      plural: 'Crianças',
      value: 0,
      minValue: 0,
      maxValue: 4
    },
    {
      title: 'Bebés (-2 anos)',
      type: 'babies',
      singular: 'Bebé',
      plural: 'Bebés',
      value: 0,
      minValue: 0,
      maxValue: 1
    },
  ]
};



export default class App {
  constructor(){
    this.component1 = new TicketsSelector(component_values);
    this.component2 = new TicketsSelector(component_values);
    return this.html();
  }

  html(){
    const div = document.createElement('div');
    div.className = 'container';
    // after component is instantiated call it's render method
    div.appendChild(this.component1.render());
    div.appendChild(this.component2.render());
    return div;
  }
}