// Add or Remove Class methods: https://jaketrent.com/post/addremove-classes-raw-javascript/
/**
 * @function hasClass checks if elements has a class already
 * @param {htmlDOMElement} element - the element to check the class
 * @param {string} className - the class to be checked
 * @return {boolean}
 */
hasClass = (element, className) => {
  return !!element.classList.contains(className);
}

/**
 * @function addClass add a class to a DOM element
 * @param {htmlDOMElement} element - the element to add the class
 * @param {string} className - the class to be added
 */
addClass = (element, className) => {
  // does the element already have the given class?
  element.className = hasClass(element, className)
    // do nothing
    ? element.className
    // add class
    : element.className += ' ' + className;
}

/**
 * @function removeClass removes class if elements has a class it
 * @param {htmlDOMElement} element - the element to remove the class
 * @param {string} className - the class to be removed
 */
removeClass = (element, className) => {
  var regexp = new RegExp('(\\s|^)' + className + '(\\s|$)');
  // does the element have the given class?
  element.className = hasClass(element, className)
    // remove class
    ? element.className.replace(regexp, '')
    // do nothing
    : element.className;
}

/**
 * @function checkClick checks if click happend outside a given element and removes active class accordingly
 */
// Remove active class upon click outside the given element
checkClick = (element) => {
  window.addEventListener('click', event => {
    !element.contains(event.target)
      // clicked outside
      ? removeClass(element, 'active')
      // clicked inside
      : element;
  });
}