'use strict';

process.env.DEFAULT_PORT = 8088; // default port to open server with
// retrive port from command arguments
process.argv.forEach(function (value, index, array) {
  const arg = value.split('=');
  if (arg[0] === '--port') {
    process.env.DEFAULT_PORT = arg[1];
  }
});

const port = process.env.DEFAULT_PORT;
const Webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const webpackConfig = require('../webpack.config');
const chalk = require('chalk');
const compiler = Webpack(webpackConfig);
const devServerOptions = Object.assign({}, webpackConfig.devServer, {
  stats: {
    colors: true
  },
  port: port
});

const server = new WebpackDevServer(compiler, devServerOptions);

server.listen(port, '127.0.0.1', () => {
  let text = chalk.bgGreen('Starting server at:') + ' ';
  text += chalk.underline(chalk.cyan(`http://localhost:${port}`)) + '\n';
  console.log(text);
});

